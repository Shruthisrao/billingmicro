package com.example.gtommee.rest_tutorial.models;

public class bills {

	private String id;
	private String href;
	private String billNo;
	private String runType;
	private String category;
	private String billDate;
	private String nextBillDate;
	private BillingPeriod billingPeriod;
	private Due amountDue;
	private String paymentDueDate;
	private Due remainingAmount;
	private Due taxExcludedAmount;
	private Due taxIncludedAmount;
	private String state;
	private String lastUpdate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public String getRunType() {
		return runType;
	}
	public void setRunType(String runType) {
		this.runType = runType;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	public String getNextBillDate() {
		return nextBillDate;
	}
	public void setNextBillDate(String nextBillDate) {
		this.nextBillDate = nextBillDate;
	}
	public BillingPeriod getBillingPeriod() {
		return billingPeriod;
	}
	public void setBillingPeriod(BillingPeriod billingPeriod) {
		this.billingPeriod = billingPeriod;
	}
	public Due getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(Due amountDue) {
		this.amountDue = amountDue;
	}
	public String getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public Due getRemainingAmount() {
		return remainingAmount;
	}
	public void setRemainingAmount(Due remainingAmount) {
		this.remainingAmount = remainingAmount;
	}
	public Due getTaxExcludedAmount() {
		return taxExcludedAmount;
	}
	public void setTaxExcludedAmount(Due taxExcludedAmount) {
		this.taxExcludedAmount = taxExcludedAmount;
	}
	public Due getTaxIncludedAmount() {
		return taxIncludedAmount;
	}
	public void setTaxIncludedAmount(Due taxIncludedAmount) {
		this.taxIncludedAmount = taxIncludedAmount;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
}
