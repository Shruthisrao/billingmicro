package com.example.gtommee.rest_tutorial.models;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
@Document(collection="billingA") 

public class Bill {
	
	private String href;
	private String ratingType;
	private String name;
	private String state;
	private creditLimit creditLimit;
	private ValidFor validFor;
	private customerAccount customerAccount;
	private customerBillingCycleSpecification customerBillingCycleSpecification;
	private customerBillFormat customerBillFormat;
	private customerBillPresentationMedia customerBillPresentationMedia;
	private currency currency;
	private List<billingAccountBalance> billingAccountBalance = new ArrayList<billingAccountBalance>();
	private List<relatedParty> relatedParty = new ArrayList<relatedParty>();
	private paymentMean paymentMean;
	private List<bills> bills = new ArrayList<bills>();
	private String _id;
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getRatingType() {
		return ratingType;
	}
	public void setRatingType(String ratingType) {
		this.ratingType = ratingType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public creditLimit getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(creditLimit creditLimit) {
		this.creditLimit = creditLimit;
	}
	public ValidFor getValidFor() {
		return validFor;
	}
	public void setValidFor(ValidFor validFor) {
		this.validFor = validFor;
	}
	public customerAccount getCustomerAccount() {
		return customerAccount;
	}
	public void setCustomerAccount(customerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}
	public customerBillingCycleSpecification getCustomerBillingCycleSpecification() {
		return customerBillingCycleSpecification;
	}
	public void setCustomerBillingCycleSpecification(customerBillingCycleSpecification customerBillingCycleSpecification) {
		this.customerBillingCycleSpecification = customerBillingCycleSpecification;
	}
	public customerBillFormat getCustomerBillFormat() {
		return customerBillFormat;
	}
	public void setCustomerBillFormat(customerBillFormat customerBillFormat) {
		this.customerBillFormat = customerBillFormat;
	}
	public customerBillPresentationMedia getCustomerBillPresentationMedia() {
		return customerBillPresentationMedia;
	}
	public void setCustomerBillPresentationMedia(customerBillPresentationMedia customerBillPresentationMedia) {
		this.customerBillPresentationMedia = customerBillPresentationMedia;
	}
	public currency getCurrency() {
		return currency;
	}
	public void setCurrency(currency currency) {
		this.currency = currency;
	}
	public List<billingAccountBalance> getBillingAccountBalance() {
		return billingAccountBalance;
	}
	public void setBillingAccountBalance(List<billingAccountBalance> billingAccountBalance) {
		this.billingAccountBalance = billingAccountBalance;
	}
	public List<relatedParty> getRelatedParty() {
		return relatedParty;
	}
	public void setRelatedParty(List<relatedParty> relatedParty) {
		this.relatedParty = relatedParty;
	}
	public paymentMean getPaymentMean() {
		return paymentMean;
	}
	public void setPaymentMean(paymentMean paymentMean) {
		this.paymentMean = paymentMean;
	}
	public List<bills> getBills() {
		return bills;
	}
	public void setBills(List<bills> bills) {
		this.bills = bills;
	}
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	
	
	

}
