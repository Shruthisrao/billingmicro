package com.example.gtommee.rest_tutorial.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.bson.types.ObjectId;
import com.example.gtommee.rest_tutorial.models.Bill;

public interface BillRepository extends MongoRepository<Bill, String>
{

	//Bill findBy_id(ObjectId id);

	//Bill findBy_id(ObjectId id);
}